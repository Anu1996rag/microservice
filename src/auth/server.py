from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from routers import auth, user
from logger import logger

import database
import models

models.Base.metadata.create_all(bind=database.engine)

app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"])


# add the routers
app.include_router(auth.router)
app.include_router(user.router)


@app.get("/")
async def auth_home():
    logger.info("home page")
    return {"message": "Authentication Service"}
